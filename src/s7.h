#ifndef S7_H
#define S7_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include <ESPAsyncUDP.h>

#include "logger.h"

#define S7_READ_ZONE_PIN 4 //D2
#define S7_WRITE_CHANGE_STATE_PIN 5
#define S7_BROADCAST_INTERVAL 500
#define S7_BROADCAST_PORT 3333
#define S7_LISTEN_PORT 3333
#define S7_TARGET "s7main"
#define S7_FROM "deviceFrom"
#define S7_DEVICE_TO_KEY "deviceTo"
#define S7_RFID_KEY_KEY "rfidKey"
#define S7_CHANGE_STATE_DOWN_DURATION 300
#define S7_ALARM_STATE_KEY "alarmState"

class S7
{
public:
    static ulong lastStatusTime;
    static AsyncUDP udp;
    static bool initCompleted;
    static ulong changeStateRequestTime;
    static u_int changeStatePin;

    static void setup()
    {
        pinMode(S7_READ_ZONE_PIN, INPUT_PULLUP);
        pinMode(S7_WRITE_CHANGE_STATE_PIN, OUTPUT);
        digitalWrite(S7_WRITE_CHANGE_STATE_PIN, LOW);
        changeStatePin = LOW;
        initCompleted = true;
        if (udp.listenMulticast(IPAddress(239,1,1,1), S7_LISTEN_PORT))
        {
            Logger::syslog("Listening on port:" + String(S7_LISTEN_PORT));
            udp.onPacket([](AsyncUDPPacket packet) {
                String packetData = "";
                for (size_t i = 0; i < packet.length(); i++)
                {
                    packetData += (char)*(packet.data() + i);
                }
                String m = "UDP Packet Type: ";
                m += packet.isBroadcast() ? "Broadcast" : packet.isMulticast() ? "Multicast" : "Unicast";
                m += ", From: " + packet.remoteIP().toString() + ":" + String(packet.remotePort()) + ", To: " + packet.localIP().toString() + ":";
                m += String(packet.localPort()) + ", Length: " + String(packet.length()) + ", Data: " + packetData;
                Logger::syslog(m);

                StaticJsonDocument<512> doc;
                DeserializationError error = deserializeJson(doc, packetData);
                if (error)
                {
                    Logger::syslog("Failed to parse JSON.");
                    return;
                }
                if (doc[S7_DEVICE_TO_KEY].isNull() || doc[S7_RFID_KEY_KEY].isNull())
                {
                    Logger::syslog("Keys S7_DEVICE_TO_KEY:" + String(S7_DEVICE_TO_KEY) + " or S7_RFID_KEY_KEY: " + String(S7_RFID_KEY_KEY) + " missing");
                    return;
                }

                if (doc[S7_DEVICE_TO_KEY].as<String>() != S7_TARGET)
                {
                    Logger::syslog("I'm not the target, dropping packet.");
                    return;
                }

                for (auto &rfid : RFID_ALLOWED)
                {
                    if (rfid.key == doc[S7_RFID_KEY_KEY].as<String>())
                    {
                        Logger::syslog("Requested to change state by :" + rfid.name + ", key: " + rfid.key);
                        if (millis() - changeStateRequestTime > S7_CHANGE_STATE_DOWN_DURATION)
                        {
                            changeStateRequestTime = millis();
                            Logger::syslog("Requested to change State");
                        }
                        else
                        {
                            Logger::syslog("Request already in progress");
                        }
                        return;
                    }
                }

                Logger::syslog("I'm unable to match received key: " + doc[S7_RFID_KEY_KEY].as<String>() + " to known users");
            });
        }
    }

    static void handle()
    {
        if (millis() - lastStatusTime > S7_BROADCAST_INTERVAL && initCompleted && WiFi.isConnected())
        {
            StaticJsonDocument<512> doc;
            doc[S7_ALARM_STATE_KEY] = digitalRead(S7_READ_ZONE_PIN) == 0 ? 1 : 0;
            doc[S7_FROM] = S7_TARGET;
            String p;
            serializeJson(doc, p);
            udp.print(p.c_str());
            lastStatusTime = millis();
        }

        if (millis() - changeStateRequestTime < S7_CHANGE_STATE_DOWN_DURATION && changeStatePin == LOW)
        {
            digitalWrite(S7_WRITE_CHANGE_STATE_PIN, HIGH);
            changeStatePin = HIGH;
            Logger::syslog("Got request, setting change state pin HIGH");
        }

        if (millis() - changeStateRequestTime > S7_CHANGE_STATE_DOWN_DURATION && changeStatePin == HIGH)
        {
            digitalWrite(S7_WRITE_CHANGE_STATE_PIN, LOW);
            changeStatePin = LOW;
            Logger::syslog("Setting change state pin LOW");
        }
    }
};

ulong S7::lastStatusTime = 0;
bool S7::initCompleted = false;
AsyncUDP S7::udp;
u_int S7::changeStatePin = LOW;
ulong S7::changeStateRequestTime = 0;
#endif