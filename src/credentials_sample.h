#ifndef CREDENTIALS_H
#define CREDENTIALS_H

#define WIFI_DEFAULT_SSID ""
#define WIFI_DEFAULT_PASS ""

struct RfidStruct {
    String name;
    String key;
    
};

const RfidStruct RFID_ALLOWED[] = {
        {"USER_NAME","RFID_KEY"},
    };


#endif