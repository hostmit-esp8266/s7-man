#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>
#include <map>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "gpioswitch.h"

//logger
#define LOGUSESERIAL true
#define LOGFILENAME "syslog.log"
#define LOGLINEMAXLENGTH 1000
#define LOGFILESIZE 10000
#define LOGPREVEXT "prev"


//gpioswitcher
#define ONBOARDLEDPIN 2
Gpioswitch builtInLed(ONBOARDLEDPIN);

//sysinfo
#define LOOPSTATSIZE 10
#define LOOPSTATDURATION 1000


//wifi section
long wifiConnectTime = 0;
long wifiConnectCount = 0;
WiFiEventHandler gotIpEventHandler, disconnectedEventHandler;
bool wifiFirstConnected;

//sys
#define ESPRESTARTDELAY 2000
#define ESPRESETDELAY 2000
bool espRestartRequest = false;
bool espResetRequest = false;

//ntpdate

#define NTPHOST "0.ua.pool.ntp.org"
#define NTPUPDATEINTERVAL 3600000
#define NTPUDPTIMEOUT 5000
#define NTPOFFSETHOURS 3

#endif